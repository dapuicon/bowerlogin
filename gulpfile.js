var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var del = require('del'); //Delete files/folders using globs
var replace = require('gulp-replace'); //String replace

var paths = {
  sass: ['./scss/**/*.scss']
};

gulp.task('default', ['sass']);

gulp.task('sass', function(done) {
  gulp.src('./scss/ionic.app.scss')
    .pipe(sass({
      errLogToConsole: true
    }))
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({
      extname: '.min.css'
    }))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});

gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass']);
});

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});

/** CUSTOM TASKS**/

/**
 * @name createDistributionPackage
 * @description
 * Crea el paquete bower a distribuit en la carpeta 'dist'
 * El orden de ejecución de las tareas gulp es:
 * -clean-dist : Eliminamos el contenido de la carpeta de distribución 'dist'
 * -move-2-dist : Movemos los ficheros que conforman el paquete bower de la carpeta 'package'
 *                a la carpeta de distribución 'dist'
 * -replacePath : Sustituimos el path que referencia a las templates utilizadas por el común
 *                utilizado en los proyectos de ionic.
 **/
gulp.task('createDistributionPackage', ['clean-dist', 'move-2-dist', 'replacePath']);

gulp.task('move-2-dist', function() {
  gulp.src('www/package/**/*')
    .pipe(gulp.dest('dist'));
});

gulp.task('clean-dist', function() {
  return del('dist/**/*');
});

gulp.task('replacePath', function() {
  gulp.src('dist/**/*.js')
    .pipe(replace(/\b(templateUrl:[ ]?['"][a-zA-Z/.]+['"])/g, function(match, p1, offset, string) {

      var plantilla = match.slice(match.indexOf("/"), match.length - 1);
      var pathBower = "lib/bowerlogin/dist";
      return "templateUrl: '" + pathBower + plantilla + "'";
    }))
    .pipe(gulp.dest('dist'));

    gulp.src('dist/**/*.html')
      .pipe(replace(/\bsrc[ ]?=[ ]?["|']package/g, function(match, p1, offset, string) {

        var plantilla = match.slice(match.indexOf("/"), match.length - 1);
        var pathBower = "lib/bowerlogin/dist";
        return "src = \"" + pathBower + plantilla;
      }))
      .pipe(gulp.dest('dist'));

});
