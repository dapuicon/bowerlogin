"use strict";
angular.module('fLogin.controllers', [])
  .controller('LogInCtrl', function($scope, $state, $ionicPopup, $ionicLoading, $http, $templateCache,
    $sce, $templateRequest, $compile, PATH,
    AuthService, CREDENTIALS) {
    // Datos del formulario para el modelo de login
    $scope.loginData = {};

    //Template
    var templateUrl = $sce.getTrustedResourceUrl('templates/loginContent.html');

    $templateRequest(templateUrl, true).then(function(template) {
        // Se ha especificado un template en la ruta definida
        $scope.templateLogin = 'templates/loginContent.html';
      },
      function(error) {
        //Cargamos la template por defecto
        $scope.templateLogin = PATH.package + '/templates/loginContent.html';
      });

    // code to run each time view is entered
    // $scope.$on('$ionicView.enter', function() {
    var credentials = AuthService.loadUserCredentials();

    if (credentials) {
      $scope.loginData.recordarPassword = true;
      $scope.loginData.username = credentials[CREDENTIALS.usuario];
      $scope.loginData.password = credentials[CREDENTIALS.password];
    }
    // });

    // Perform the login action when the user submits the login form
    $scope.doLogin = function(authForm) {
      if (authForm.$valid) {
        // Setup the loader
        $ionicLoading.show({
          content: 'Cargando',
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });

        console.log('Haciendo login', $scope.loginData);
        AuthService.login($scope.loginData.username, $scope.loginData.password, $scope.loginData.recordarPassword)
          .then(function(authenticated) {
            //Reseteamos el estado del formulario
            authForm.$setPristine();

            //Si no hay que recordar la contraseña limpiamos los datos del formulario
            if (!$scope.loginData.recordarPassword) $scope.loginData = {};

            //Redirigimos hacia donde se iba antes de loguearse
            var nextState = window.sessionStorage.getItem('MAIN_PAGE');
            var nextStateParams = JSON.parse(window.sessionStorage.getItem('MAIN_PAGE_PARAMS'));
            $state.go(nextState, nextStateParams);
          }, function(err) {

            var alertPopup = $ionicPopup.alert({
              title: err.title,
              template: err.description,
              okType: 'button-balanced'
            });

          }).finally(function() {
            $ionicLoading.hide();
          });
      }


    };

    $scope.doLogOut = function() {
      AuthService.logout();
    };

  });
