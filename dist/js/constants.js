"use strict";
angular.module('fLogin.constants', [])
.constant('AUTH_EVENTS', {
  notAuthenticated: 'auth-not-authenticated',
  authenticated: 'auth-authenticated'
})

.constant('HEADER_VALUES', {
  accessToken: 'X-Access-Token'
})

.constant('URLS',{
  // autenticacion: 'http://localhost:51892'
  autenticacion: 'http://newton-dx.fraternidad.local:94/fraternidad.app.arq.logon.web'
})

.constant('PATH',{
  //package: 'package'
  package: 'lib/bowerlogin/dist'
})

.constant('CREDENTIALS',{
  usuario: 'usuario',
  password: 'pasw'
})

.value('APP_CONFIG', {
  ttlToken: '100',
  emisor: 'FraternidadArq'
})

;
