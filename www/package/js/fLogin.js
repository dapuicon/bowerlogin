"use strict";
angular.module('fLogin', ['fLogin.services', 'fLogin.controllers', 'fLogin.constants'])
  .config(function($stateProvider, $httpProvider, $ionicConfigProvider, PATH) {
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: PATH.package + '/templates/login.html',
        controller: 'LogInCtrl'
      });
    //Registramos es el interceptor de las peticiones http
    //que se ecarga de setear y guardar el token de autenticación
    $httpProvider.interceptors.push('AuthInterceptor');
  })

.run(function($rootScope, $state, AuthService, AUTH_EVENTS, $ionicPopup) {
  $rootScope.$on('$stateChangeStart', function(event, next, nextParams, fromState) {

    if (!AuthService.isAuthenticated()) {
      if (next.name !== 'login') {
        window.sessionStorage.setItem('MAIN_PAGE', next.name); //save for redirect after successful login
        window.sessionStorage.setItem('MAIN_PAGE_PARAMS', JSON.stringify(nextParams));
        // $sessionStorage.toParams = nextParams; //save for redirect after successful login

        event.preventDefault();
        $state.go('login');
      }
    }
  });

  $rootScope.$on(AUTH_EVENTS.notAuthenticated, function(event) {
    AuthService.destroyUserCredentials();

    var alertPopup = $ionicPopup.alert({
      title: 'Sesión cerrada',
      //template: 'Lo sentimos, por favor vuelva a autenticarse',
      okType: 'button-balanced'
    });

    $state.go('login');
  });
})

.directive('showWhen', ['$window', function($window) {


 return {
    restrict: 'A',
    link: function($scope, $element, $attr) {

  function checkExpose() {
    var mq = $attr.showWhen == 'large' ? '(min-width:768px)' : $attr.showWhen;
    if($window.matchMedia(mq).matches){
		$element.removeClass('ng-hide');
	} else {
		$element.addClass('ng-hide');
	}
  }

  function onResize() {
    debouncedCheck();
  }

  var debouncedCheck = ionic.debounce(function() {
    $scope.$apply(function(){
      checkExpose();
    });
  }, 300, false);

  checkExpose();

  ionic.on('resize', onResize, $window);

  $scope.$on('$destroy', function(){
    ionic.off('resize', onResize, $window);
  });

}
  };
}])
;
