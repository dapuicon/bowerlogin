"use strict";
angular.module('fLogin.services', [])
  .service('AuthService', function($q, $http, APP_CONFIG,
    UserCredentialsService, AUTH_EVENTS, $rootScope, URLS) {
    //Varibales internas
    var isAuthenticated = false;
    //Llamadas a métodos
    //loadUserCredentials();

    //Funciones internas del servicio
    function loadUserCredentials() {
      return UserCredentialsService.getCredentials();
    }

    function destroyUserCredentials() {
      UserCredentialsService.setAuthenticationToken(undefined);
      isAuthenticated = false;
    }

    var login = function(name, pw, recordarPassword) {
      return $q(function(resolve, reject) {

        var url = URLS.autenticacion + '/Autenticacion.svc/login';
        var data = {
          "UserName": name,
          "Password": pw,
          "TtlToken": APP_CONFIG.ttlToken || "100",
          "Emisor": APP_CONFIG.emisor || "portal.paciente"
        };

        console.info("URL: " + url);
        console.info("DATA: ", data);

        $http.post(url, data)
          .then(function successCallback(response) {
            if (recordarPassword) {
              UserCredentialsService.storeCredentials(name, pw);
            } else {
              UserCredentialsService.destroyCredentials();
            }
            console.info("Success - Response received");
            console.info(response);
            isAuthenticated = true;
            resolve('Logeado correctamente.');
          }, function errorCallback(response) {
            console.error("Error");
            console.error(response);
            isAuthenticated = false;
            reject({
              title: 'Login Fallido',
              description: 'Revisa las credenciales' + response
            });
          });
      });
    };

    //No hace falta enviar petición al server ya que no es como una cookie que se almacena en él
    var logout = function() {
      $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
    };
    //Propiedades y métodos del servicio
    return {
      login: login,
      logout: logout,
      destroyUserCredentials: destroyUserCredentials,
      isAuthenticated: function() {
        return isAuthenticated;
      },
      loadUserCredentials: loadUserCredentials
    };
  })

.service('AuthInterceptor', function($rootScope, $q, UserCredentialsService, AUTH_EVENTS, HEADER_VALUES) {
  var service = this;
  //var authService = $injector.get('AuthService');

  service.request = function(config) {
    var token = UserCredentialsService.getAuthenticationToken();
    console.debug("AuthInterceptor => Token a Enviar:" + token);
    // Seteamos el token en la cabecera para nuestras peticiones
    if (token) {
      config.headers[HEADER_VALUES.accessToken] = token;
    }
    return config;
  };

  service.response = function(response) {
    var token = response.headers(HEADER_VALUES.accessToken);

    console.debug("AuthInterceptor => Token recibido: " + token);
    if (token) UserCredentialsService.setAuthenticationToken(token);
    return response;
  };

  service.responseError = function(response) {
    console.debug("AuthInterceptor => ResponseCode:" + response.status);
    $rootScope.$broadcast({
      401: AUTH_EVENTS.notAuthenticated
    }[response.status], response);

    return $q.reject(response);
  };
})

.service('UserCredentialsService', function(CREDENTIALS) {
  var LOCAL_TOKEN_KEY = 'AutenticationTokenKey';
  var authen_token;

  var setAuthenticationToken = function(token) {
    if (token) {
      authen_token = token;
      window.localStorage.setItem(LOCAL_TOKEN_KEY, token);
    } else {
      authen_token = undefined;
      window.localStorage.removeItem(LOCAL_TOKEN_KEY);
    }
  };

  var getAuthenticationToken = function() {
    console.debug('UserCredentialsService => authen_token: ' + authen_token);
    console.debug('UserCredentialsService => localStorage: ' + window.localStorage.getItem(LOCAL_TOKEN_KEY));
    return authen_token ? authen_token : window.localStorage.getItem(LOCAL_TOKEN_KEY);
  };

  var storeCredentials = function(name, pw) {
    window.localStorage.setItem(CREDENTIALS.usuario, name);
    window.localStorage.setItem(CREDENTIALS.password, pw);
  };

  var destroyCredentials = function() {
    window.localStorage.removeItem(CREDENTIALS.usuario);
    window.localStorage.removeItem(CREDENTIALS.password);
  };

  var getCredentials = function() {
    var user = window.localStorage.getItem(CREDENTIALS.usuario);
    var pw = window.localStorage.getItem(CREDENTIALS.password);

    var resultado;
    if (user && pw) {
      resultado = new Object();
      resultado[CREDENTIALS.usuario] = user;
      resultado[CREDENTIALS.password] = pw;
    }
    return resultado;
  };

  return {
    getAuthenticationToken: getAuthenticationToken,
    setAuthenticationToken: setAuthenticationToken,
    storeCredentials: storeCredentials,
    getCredentials: getCredentials,
    destroyCredentials: destroyCredentials
  };
});
