angular.module('starter.services', [])

.service('SearchService', function($q,$http){
  var peticion = function() {
    return $q(function(resolve, reject) {

      var url = 'http://newton-dx.fraternidad.local:94/fraternidad.app.arq.test.web/ValuesService.svc/values';
      //var url = 'http://localhost:62389/ValuesService.svc/values';
      $http.get(url)
        .then(function successCallback(response) {
          resolve(response.data);
        }, function errorCallback(response) {
          reject({
            title: 'Petición fallida',
            description: response.data
          });
        });
    });
  };

  var peticion2 = function() {
    return $q(function(resolve, reject) {

      var url = 'http://newton-dx.fraternidad.local:94/fraternidad.app.arq.test.web/ValuesService.svc/procesos/1234?numpag=1';
      //var url = 'http://localhost:62389/ValuesService.svc/procesos/1234?numpag=1';
      $http.get(url)
        .then(function successCallback(response) {
          resolve(response.data);
        }, function errorCallback(response) {
          reject({
            title: 'Petición fallida',
            description: response.data
          });
        });
    });
  };

  return {
    peticion : peticion,
    peticion2 : peticion2
  };
});
