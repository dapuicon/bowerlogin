angular.module('starter.controllers', ['starter.services'])

//.controller('LogInCtrl', function($scope, $state, $ionicPopup, AuthService)) {

.controller('SearchCtrl', function($scope, SearchService, $ionicPopup) {
  $scope.sendRequest = function() {
    SearchService.peticion().then(function(data) {
      var alertPopup = $ionicPopup.alert({
        title: "Respuesta",
        template: data,
        okType: 'button-balanced'
      });
    }, function(err) {
      var alertPopup = $ionicPopup.alert({
        title: err.title,
        template: err.description,
        okType: 'button-balanced'
      });
    });
  };

  $scope.obtener = function() {
    SearchService.peticion2().then(function(data) {
      console.log(data);
      var alertPopup = $ionicPopup.alert({
        title: "Respuesta",
        template: data,
        okType: 'button-balanced'
      });
    }, function(err) {
      var alertPopup = $ionicPopup.alert({
        title: err.title,
        template: err.description,
        okType: 'button-balanced'
      });
    });
  };
})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [{
    title: 'Reggae',
    id: 1
  }, {
    title: 'Chill',
    id: 2
  }, {
    title: 'Dubstep',
    id: 3
  }, {
    title: 'Indie',
    id: 4
  }, {
    title: 'Rap',
    id: 5
  }, {
    title: 'Cowbell',
    id: 6
  }];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {})

.controller('MenuCtrl', function($scope, $rootScope, AuthService) {
  $scope.logOut = function() {
    AuthService.logout();
  }
})

.controller('LoginContentCtrl', function($scope) {
var self=this;
  self.msg = "Controller is Working properly";
})

;
